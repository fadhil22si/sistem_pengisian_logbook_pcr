<html class="no-js" lang="en">

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="ThemeMarch">
    <!-- Favicon Icon -->
    <link rel="icon" href="{{ asset('CALTEX-RIAU-LOGO.png') }}">
    <!-- Site Title -->
    <title>Landing</title>
    <link rel="stylesheet" href="{{ asset('assetsLanding/css/plugins/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assetsLanding/css/plugins/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('assetsLanding/css/plugins/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assetsLanding/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
        integrity="sha384-rp5p/6u+HtYYeIwLthETeflCUdEcAl1z9vDZZQRHE8tvW6FQNK5IRG9e9GXs2QfP" crossorigin="anonymous">
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        h1 {
            font-weight: bold;
        }
    </style>

</head>

<body>
    <div class="cs-preloader cs-white_bg cs-center" style="display: none;">
        <div class="cs-preloader_in" style="display: none;">
            <img src="{{ asset('assetsLanding/img/logo_mini.svg') }}" alt="Logo">
        </div>
    </div>

    <!-- Start Header Section -->
    <header class="cs-site_header cs-style1 cs-sticky-header cs-primary_color cs-white_bg">
        <div class="cs-main_header">
            <div class="container">
                <div class="cs-main_header_in">
                    <div class="cs-main_header_left">
                        <a class="cs-site_branding cs-accent_color" href="{{ url('/') }}">
                            <img src="{{ asset('Politeknik_Caltex_Riau.png') }}" alt="Logo"
                                style="width: 255px; height: auto;">
                        </a>

                    </div>
                    <div class="cs-main_header_center">

                    </div>
                    <div class="cs-main_header_right">
                        <a href="{{ route('login') }}" class="cs-toolbox">
                            <span class="cs-btn cs-color1"><span>Login</span></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="home">
        <div class="cs-height_50 cs-height_lg_50"></div>
        <section class="cs-hero cs-style1 cs-bg" data-src="{{ asset('assetsLanding/img/hero_bg.svg') }}"
            style="background-image: url('{{ asset('assetsLanding/img/hero_bg.svg') }}');">
            <div class="container">
                <div class="cs-hero_img">
                    <div class="cs-hero_img_bg cs-bg" data-src="{{ asset('assetsLanding/img/hero_img_bg.png') }}"
                        style="background-image: url('{{ asset('assetsLanding/img/hero_img_bg.png') }}');"></div>
                    <img src="{{ asset('assetsLanding/img/buku.png') }}" alt="Hero Image" class="wow fadeInRight"
                        data-wow-duration="1s" data-wow-delay="0.4s"
                        style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s; animation-name: fadeInRight;">
                </div>
                <div class="cs-hero_text">
                    <h1>Sistem Pengisian <br>Logbook PCR</h1>
                    <div class="cs-hero_subtitle">Silahkan Mengisi logbook terlebih dahulu sebelum <br> melakukan
                        perkuliahan
                        atau kegiatan
                    </div>
                    <a href="{{ route('kuliah.create') }}" class="cs-btn"><i class="fas fa-school"> </i><span>Log
                            Perkuliahan</span></a>
                    <a href="{{ route('Lkegiatan.create') }}" class="cs-btn"><i
                            class="fas fa-calendar-alt"></i><span>Log
                            Kegiatan</span> </a>
                </div>
                <div class="cs-hero_shapes">
                    <div class="cs-shape cs-shape_position1">
                        <img src="{{ asset('assetsLanding/img/shape/shape_1.svg') }}" alt="Shape">
                    </div>
                    <div class="cs-shape cs-shape_position2">
                        <img src="{{ asset('assetsLanding/img/shape/shape_2.svg') }}" alt="Shape">
                    </div>
                    <div class="cs-shape cs-shape_position3">
                        <img src="{{ asset('assetsLanding/img/shape/shape_3.svg') }}" alt="Shape">
                    </div>
                    <div class="cs-shape cs-shape_position4">
                        <img src="{{ asset('assetsLanding/img/shape/shape_4.svg') }}" alt="Shape">
                    </div>
                    <div class="cs-shape cs-shape_position5">
                        <img src="{{ asset('assetsLanding/img/shape/shape_5.svg') }}" alt="Shape">
                    </div>
                    <div class="cs-shape cs-shape_position6">
                        <img src="{{ asset('assetsLanding/img/shape/shape_6.svg') }}" alt="Shape">
                    </div>
                </div>
            </div>
        </section>
    </div>


    <section class="cs-bg" data-src="{{ asset('assetsLanding/img/feature_bg.svg') }}" id="pricing"
        style="background-image: url('{{ asset('assetsLanding/img/feature_bg.svg') }}');">
    </section>

    <!-- Script -->
    <script src="{{ asset('assetsLanding/js/plugins/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('assetsLanding/js/plugins/jquery.slick.min.js') }}"></script>
    <script src="{{ asset('assetsLanding/js/plugins/jquery.counter.min.js') }}"></script>
    <script src="{{ asset('assetsLanding/js/plugins/wow.min.js') }}"></script>
    <script src="{{ asset('assetsLanding/js/main.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        @if (session('success'))
            Swal.fire({
                icon: 'success',
                title: '{{ session('success') }}',
                showConfirmButton: false,
                timer: 1100
            });
        @elseif(session('error'))
        Swal.fire({
            icon: 'error',
            title: '{{ session('error') }}',
            showConfirmButton: false,
            timer: 1100
        });
        @endif
    </script>

</body>

</html>