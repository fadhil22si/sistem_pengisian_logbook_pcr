<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    public function handle(Request $request, Closure $next, ...$roles)
    {
        // Check if the authenticated user has the required role
        if (!auth()->check() || !in_array(auth()->user()->level, $roles)) {
            abort(403, 'Unauthorized action.'); // Redirect or display an error message
        }

        return $next($request);
    }
}
