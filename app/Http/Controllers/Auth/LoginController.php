<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('Auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (auth()->attempt($credentials)) {
            // Jika login berhasil
            $user = auth()->user();
            session(['username' => $user->username]);

            if ($user->level === 'dosen') {
                return redirect()->route('admin.index'); // Ganti dengan nama route yang sesuai
            } elseif ($user->level === 'ail') {
                return redirect()->route('admin.index'); // Ganti dengan nama route yang sesuai
            } elseif ($user->level === 'superadmin') {
                return redirect()->route('superadmin.dashboard'); // Ganti dengan nama route yang sesuai
            }
        }

        // Jika login gagal, kembalikan ke halaman login dengan pesan error
        return redirect()->route('login')->with('error', 'Login failed. Please check your credentials.');
    }

    public function logout()
    {
        auth()->logout();

        return redirect('/');
    }
}
