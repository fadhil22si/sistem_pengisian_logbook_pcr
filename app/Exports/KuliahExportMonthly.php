<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class KuliahExportMonthly implements FromCollection, WithHeadings
{

    public function collection()
    {
        $currentDate = now();
        $lastMonthDate = now()->subMonth();

        $data = DB::table('logkuliah')
            ->join('lab', 'logkuliah.id_lab', '=', 'lab.id_lab')
            ->join('kelas', 'logkuliah.kelas', '=', 'kelas.id_kelas')
            ->select(
                'logkuliah.nama',
                'logkuliah.nim',
                'kelas.nama_kelas',
                'logkuliah.pc',
                'lab.ruang_lab',
                'logkuliah.jamMasuk',
                'logkuliah.jamKeluar',
                'logkuliah.tanggal',
                'logkuliah.keterangan',
                'logkuliah.alat',
                'logkuliah.mouse',
                'logkuliah.keyboard',
                'logkuliah.monitor',
                'logkuliah.jaringan',
                'logkuliah.durasi'
                // ... tambahkan atribut lain sesuai kebutuhan
            )
            ->whereNull('SKS')
            ->whereBetween('logkuliah.tanggal', [$lastMonthDate, $currentDate])
            ->orderBy('logkuliah.id_logkul', 'desc')
            ->get();

        return $data;
    }

    public function headings(): array
    {
        return [
            'Nama',
            'NIM',
            'Kelas',
            'Nomor PC',
            'Ruang Lab',
            'Jam Masuk',
            'Jam Keluar',
            'Tanggal',
            'Keterangan',
            'Alat',
            'Mouse',
            'Keyboard',
            'Monitor',
            'Jaringan',
            'Durasi'
            // ... tambahkan atribut lain sesuai kebutuhan
        ];
    }

}
